# EMT Conduit Connectors

## Introduction

This is a set of OpenSCAD scripts for generating various connectors for EMT
conduit. Inspired by [MakerPipe][mp], these are designed to help you prototype
out a build with EMT conduit at full scale, or alternatively for use when you
don't need the structural support that would come from having metal connectors.

Note that none of these have been load tested, and the default settings are
largely what "felt right", so don't rely on these prints (especially in weaker
materials) to provide any significant strength or load bearing capabilities.

## Getting Started

You will need [OpenSCAD], version 2019.5 or later in order to use the
customizer features. Open the [emt_connector.scad][main] file. In the customizer
you can choose which connector type you want to render, the inner diameter for
the connector and how much of the pipe the connector will accept.

The `Rendering Details` section allows you to customize the values being used by
OpenSCAD during its rendering. Setting the segment length to roughly half your
nozzle width should provide a very smooth model for your slicer.

## Licensing

In the interests of furthering open hardware and software, this project both in
hardware and software form is dual licensed under the CERN Open Hardware Licence
Version 2 - Strongly Reciprocal only and the APGL v3 Only
as listed in [LICENSE.txt]. However, I recognize that there are a number of
positive and useful projects available under more permissive licenses or commercial
ones. To that end, I also offer licensing under other terms. Please contact
me at [licensing@ncitguys.com] to discuss your uses and options for alternate
licensing.

[LICENSE.txt]: LICENSE.txt
[licensing@ncitguys.com]: <mailto:licensing@ncitguys.com?subject=Alternate Licensing Request for EMT Conduit Connectors>
[main]: emt_connector.scad
[mp]: https://makerpipe.com
[openscad]: https://openscad.org
