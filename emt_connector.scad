/**
Copyright © 2023 Tevis Money
Dual Licensed under CERN-OHL-S v2 only and AGPL v3 only

This project is free software and hardware and you should have received a
LICENSE.txt file with one or both of the above licenses when you received this
project. You may modify and distribute this project under the terms of those
licenses.

This software is available under more permissive licensing terms. Contact
licensing@ncitguys.com to for more information.
**/

use <lib/end_mount.scad>
use <lib/fabric_clamp.scad>
use <lib/ninety-degree-tee.scad>
use <lib/ninety-degree.scad>
use <lib/one-eighty-degree.scad>
use <lib/tee.scad>
/* clang-format off */

/* [Common Properties] */

// What connector type to render
connector = "Tee";// [Tee, 90 Degree, 90 Degree Tee, 180 Degree, 180 Degree Tee, 180 Degree Star, Fabric Clamp, End Mount]

// The (Nominal) EMT conduit size to accept
inner_diameter = 17.9;// [17.9: 16 mm / ½ in, 23.4: 21 mm / ¾ in, 29.5: 27mm / 1 in, 38.4: 35mm / 1¼ in, 44.2: 41mm / 1½ in, 55.8: 53 mm / 2 in, 73: 63mm / 2½ in, 88.9: 78mm / 3 in, 101.6: 91mm / 3½ in, 114.3: 103mm / 4 in]

// Instead of a fixed value from above, specify your own diameter. Use 0 to turn off
manual_diameter = 0;

// How much conduit should go into the connector, or length if Fabric Clamp (should always be AT LEAST the diameter length)
depth = 25;

// How much extra slop in the inner diameter (in mm)
inner_tolerance = 0.50;

/* [180 Degreee Connector Options] */

// Whether the core should be hollow or solid
solid_core = false;

/* [Fabric Clamp Options] */

// How many screw holes to run along the length of the clamp
screw_count = 3;

// Generate clamping tabs
clamp_edge = true;

/* [End Mount Options] */

// Make the mount "half open" so the pipe can be lifted into or out of the mount without pulling it laterally
half_open = false;

/* [ Rendering Details ] */

// The segment length to use when rendering in preview mode
preview_segment_length=1;
// The minimum arc degrees per segment when rendering in preview mode
preview_arc_degrees=5;
// The segment length to use when rendering for export. A good value would be half your nozzle width for 3d printers
export_segment_length=0.16;
// The minimum arc degrees per segment when rendering for export. 1 will give good resolution for all the models
export_arc_degrees=1;

$fs = $preview ? preview_segment_length : export_segment_length;
$fa = $preview ? preview_arc_degrees : export_arc_degrees;

/* clang-format on */

final_diameter = manual_diameter > 0 ? manual_diameter : inner_diameter;

if (connector == "Tee") {
  tee(final_diameter, depth, inner_tolerance = inner_tolerance);
} else if (connector == "90 Degree") {
  ninety_degree(final_diameter, depth, inner_tolerance = inner_tolerance);
} else if (connector == "90 Degree Tee") {
  ninety_degree_tee(final_diameter, depth, inner_tolerance = inner_tolerance);
} else if (connector == "180 Degree") {
  one_eighty_degree(4, final_diameter, depth, solid_core,
                    inner_tolerance = inner_tolerance);
} else if (connector == "180 Degree Tee") {
  one_eighty_degree(5, final_diameter, depth, solid_core,
                    inner_tolerance = inner_tolerance);
} else if (connector == "180 Degree Star") {
  one_eighty_degree(6, final_diameter, depth, solid_core,
                    inner_tolerance = inner_tolerance);
} else if (connector == "Fabric Clamp") {
  fabric_clamp(final_diameter, depth, inner_tolerance = inner_tolerance,
               clamp_edge = clamp_edge, boss_count = screw_count);
} else if (connector == "End Mount") {
  end_mount(final_diameter, depth, half_open = half_open,
            inner_tolerance = inner_tolerance);
}
