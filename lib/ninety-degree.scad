/**
Copyright © 2023 Tevis Money
Dual Licensed under CERN-OHL-S v2 only and AGPL v3 only

This project is free software and hardware and you should have received a
LICENSE.txt file with one or both of the above licenses when you received this
project. You may modify and distribute this project under the terms of those
licenses.

This software is available under more permissive licensing terms. Contact
licensing@ncitguys.com to for more information.
**/

use <common_components.scad>

/**
# 90 Degree Connector
Creates a new 90 degree connector

# Usage
ninety_degree([diameter], [depth], [radius], [wall], [wall_round_up],
  [screw_size])

# Arguments

* `diameter` - The inner diameter of the connector. Default: `20`

* `depth` - The "sleeve" depth for the connector, this is how much of the
            pipe being connected should fit inside the connector.
            Default: `20`

* `radius` - The radius of the 90 degree bend. Default: `outside diameter / 4`

* `wall` - How thick the connector walls should be. Default: `3`

* `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                    the next highest integer value. For example if the
                    diameter is `3.25`, and the wall thickness is `3` then the
                    outer diameter of the connector would be `7` instead of the
                    `6.25` that it would normally be. Default: `false`

* `screw_size` - How large of a screw the screw boss should support, set
                 to　0 to remove screw bosses. Default: `3`

* `inner_tolerance` - How much extra slop to add to the inner diameter.
                      Default: `0`
**/
module ninety_degree(diameter = 20, depth = 20, radius = -1, wall = 3,
                     wall_round_up = false, screw_size = 3,
                     inner_tolerance = 0) {
  difference() {
    main_body(diameter, depth, radius, wall, wall_round_up, screw_size,
              inner_tolerance, false);
    main_body(diameter, depth, radius, wall, wall_round_up, screw_size,
              inner_tolerance, true);
  }

  // Construct the main body, or it's drills
  module main_body(diameter, depth, radius, wall, wall_round_up, screw_size,
                   inner_tolerance, drill) {
    diameters = tube_diameters(diameter, wall, wall_round_up, inner_tolerance);

    radius = radius < 0 ? (diameters[1] + wall) / 4 : radius;
    elbow(radius, diameters, drill);

    // First tube
    translate([ radius + diameters[1], 0, 0 ]) {
      rotate([ 90, 0, 0 ]) {
        tube_and_drill(diameter, depth, wall, wall_round_up, 2, screw_size,
                       inner_tolerance, drill);
      }
    }

    // Second tube
    translate([ 0, radius + diameters[1], 0 ]) {
      rotate([ 90, 0, -90 ]) {
        tube_and_drill(diameter, depth, wall, wall_round_up, 2, screw_size,
                       inner_tolerance, drill);
      }
    }
  }

  // Construct the elbow of the 90 degree connector
  module elbow(radius, diameters, drilled) {
    rotate_extrude(angle = 90) {
      translate([ radius + diameters[1], 0, 0 ]) {
        if (!drilled) {
          circle(diameters[1] / 2);
        } else {
          color("red") { circle(diameters[0] / 2); }
        }
      }
    }
  }
}
