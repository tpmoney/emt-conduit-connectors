/**
Copyright © 2023 Tevis Money
Dual Licensed under CERN-OHL-S v2 only and AGPL v3 only

This project is free software and hardware and you should have received a
LICENSE.txt file with one or both of the above licenses when you received this
project. You may modify and distribute this project under the terms of those
licenses.

This software is available under more permissive licensing terms. Contact
licensing@ncitguys.com to for more information.
**/

use <common_components.scad>

/**
# 180 Degree Connector
Creates a new 180 degree "+" style connector

# Usage
one_eighty_degree([arm_count], [diameter], [depth], [solid_core], [wall],
                  [screw_size], [wall_round_up])

# Arguments

* `arm_count` - The number of arms this connector will have. Can be either:
                * 4 for a 180 degree straight connector (+x, -x, +z, -z)
                * 5 for a 180 degree "tee" connector (add -y)
                * 6 for a 180 degree "star" connector (add +y)
                Default: 4

* `diameter` - The inner diameter of the connector. Default: `20`

* `depth` - The "sleeve" depth for the connector, this is how much of the
            pipe being connected should fit inside the connector.
            Default: `20`

* `solid_core` - If `true` the center of the connector  will be solid instead
                 of hollow. Default: `false`

* `wall` - How thick the connector walls should be. Default: `3`

* `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                    the next highest integer value. For example if the
                    diameter is `3.25`, and the wall thickness is `3` then
                    the outer diameter of the connector would be `7` instead
                    of the `6.25` that it would normally be. Default: `false`

* `screw_size` - How large of a screw the screw boss should support, set
                 to　0 to remove screw bosses. Default: `3`

* `inner_tolerance` - How much extra slop to add to the inner diameter.
                      Default: 0
**/
module one_eighty_degree(arm_count = 4, diameter = 20, depth = 20,
                         solid_core = false, wall = 3, wall_round_up = false,
                         screw_size = 3, inner_tolerance = 0) {

  assert(arm_count >= 4, "Arm count must be at least 4");
  assert(arm_count <= 6, "Arm count must be at most 6");
  difference() {
    main_body(arm_count, diameter, depth, solid_core, wall, wall_round_up,
              screw_size, inner_tolerance, false);
    main_body(arm_count, diameter, depth, solid_core, wall, wall_round_up,
              screw_size, inner_tolerance, true);
  }

  // Construct the main body, or its drills
  module main_body(arm_count, diameter, depth, solid_core, wall, wall_round_up,
                   screw_size, inner_tolerance, drill) {

    diameters = tube_diameters(diameter, wall, wall_round_up, inner_tolerance);
    if (!drill || !solid_core) {
      one_eighty_degree_core(arm_count, diameter, wall, wall_round_up,
                             inner_tolerance, drill);
    }

    one_eighty_degree_arms(arm_count, diameter, depth, wall, wall_round_up, 2,
                           screw_size, inner_tolerance, drill);
  }

  /**
  # 180 Degree Core
  Creates a "core" body for 180 degree connectors with a variable number of arms

  # Usage
  `one_eighty_degree_core(arm_count, [diameters], [wall], [wall_round_up],
                          [drill])`

  # Arguments

  * `arm_count` - The number of arms the core should support, must be one of
                  4, 5 or 6

  * `diameter` - The inner diameter of the tube. Default: `20`

  * `wall` - How thick the tube walls should be. Default: `3`

  * `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                      the next highest integer value. For example if the
                      diameter is `3.25`, and the wall thickness is `3` then
                      the outer diameter of the tube would be `7` instead of the
                      `6.25` that it would normally be. Default: `false`

  * `inner_tolerance` - How much extra slop to add to the inner diameter.
                        Default: 0

  * `drill` - If `true` will generate the 'drill' object for using in a
              difference
  **/
  module one_eighty_degree_core(arm_count, diameter = 20, wall = 3,
                                wall_round_up = false, inner_tolerance = 0,
                                drill = false) {

    assert(arm_count >= 4, "Arm count must be at least 4");
    assert(arm_count <= 6, "Arm count must be at most 6");
    diameters = tube_diameters(diameter, wall, wall_round_up, inner_tolerance);
    tube_len = diameters[1];
    shift = tube_len / 2;

    // Arms 1 & 2
    tube_and_drill(diameter, tube_len, wall, wall_round_up,
                   inner_tolerance = inner_tolerance, drill = drill);

    // Arms 3 & 4
    translate([ -shift, 0, shift ]) {
      rotate([ 0, 90, 0 ]) {
        tube_and_drill(diameter, tube_len, wall, wall_round_up,
                       inner_tolerance = inner_tolerance, drill = drill);
      }
    }

    // Arm 5
    if (arm_count > 4) {
      translate([ 0, 0, diameters[1] / 2 ]) {
        rotate([ 90, 90, 0 ]) {
          tube_and_drill(diameter, tube_len / 2, wall, wall_round_up,
                         inner_tolerance = inner_tolerance, drill = drill);
        }
      }
    }

    // Arm 6
    if (arm_count > 5) {
      translate([ 0, 0, diameters[1] / 2 ]) {
        rotate([ -90, -90, 0 ]) {
          tube_and_drill(diameter, tube_len / 2, wall, wall_round_up,
                         inner_tolerance = inner_tolerance, drill = drill);
        }
      }
    }
  }

  /**
  # 180 Degree Arms
  Creates the arms for 180 degree connectors with a variable number of arms

  # Usage
  `one_eighty_degree_arms(arm_count, [diameters], [length] [wall],
  [wall_round_up], [drill])`

  # Arguments

  * `arm_count` - The number of arms to generate should support, must be one of
                  4, 5 or 6

  * `diameter` - The inner diameter of the tube. Default: `20`

  * `length` - The length of the tube. Default: `20`

  * `wall` - How thick the tube walls should be. Default: `3`

  * `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                      the next highest integer value. For example if the
                      diameter is `3.25`, and the wall thickness is `3` then
                      the outer diameter of the tube would be `7` instead of the
                      `6.25` that it would normally be. Default: `false`

  * `boss_count` - The number of screw bosses to place on either side of the
                   tube. For example, if `2` then the tube will have 4 bosses
                   total, 2 in a row, and another row of 2 180º opposite. Bosses
                   are placed evenly along the length of the tube. Default: 0

  * `boss_size` - The size of the screw the screw bosses should accomodate.
                  Default: 3

  * `inner_tolerance` - How much extra slop to add to the inner diameter.
                        Default: 0

  * `drill` - If `true` will generate the 'drill' object for using in a
  difference
  **/
  module one_eighty_degree_arms(
      arm_count, diameter = 20, length = 20, wall = 3, wall_round_up = false,
      boss_count = 0, boss_size = 3, inner_tolerance = 0, drill = false) {
    assert(arm_count >= 4, "Arm count must be at least 4");
    assert(arm_count <= 6, "Arm count must be at most 6");
    diameters = tube_diameters(diameter, wall, wall_round_up, inner_tolerance);

    half_diam = diameters[1] / 2;
    arm1_z_shift = diameters[1];
    arm2_z_shift = -length;
    arm4_x_shift = -length - diameters[1] / 2;
    arm5_y_shift = -half_diam;

    // clang-format off
    // Positions [translation, rotation]
    positions = [
      [[0, 0, arm1_z_shift], [0, 0, 90]],
      [[0, 0, arm2_z_shift], [0, 0, 90]],
      [[half_diam, 0, half_diam], [0, 90, 0]],
      [[arm4_x_shift, 0, half_diam], [0, 90, 0]],
      [[0, arm5_y_shift, half_diam], [90, 0, 0]],
      [[0, half_diam, half_diam], [-90, 0, 0]]
    ];
    // After the initial rotations, we need to rotate each tube to align the
    // screw bosses along the Y axis for the main arms, and along a 45 degree
    // angle for the T arm
    boss_rotate = [
      [0, 0, 0],
      [0, 0, 0],
      [90, 0, 0],
      [90, 0, 0],
      [0, 45, 0],
      [0, 45, 0]
    ];
    // clang-format on

    for (cv = [0:arm_count - 1]) {
      translate(positions[cv][0]) {
        rotate(boss_rotate[cv]) {
          rotate(positions[cv][1]) {
            tube_and_drill(diameter, length, wall, wall_round_up, boss_count,
                           boss_size, inner_tolerance, drill);
          }
        }
      }
    }
  }
}
