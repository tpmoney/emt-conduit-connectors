/**
Copyright © 2023 Tevis Money
Dual Licensed under CERN-OHL-S v2 only and AGPL v3 only

This project is free software and hardware and you should have received a
LICENSE.txt file with one or both of the above licenses when you received this
project. You may modify and distribute this project under the terms of those
licenses.

This software is available under more permissive licensing terms. Contact
licensing@ncitguys.com to for more information.
**/

/**
Calculate the necessary diameters for a tube from given properties

# Arguments

* `diameter` - The inner diameter of the tube

* `wall` - How thick the tube walls should be

* `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                    the next highest integer value. For example if the
                    diameter is `3.25`, and the wall thickness is `3` then the
                    outer diameter of the tube would be `7` instead of the
                    `6.25` that it would normally be

* `inner_tolerance` - How much extra slop to add to the inner diameter.
                      Default: 0

# Returns
[inner_diameter, outer_diameter]
**/
function tube_diameters(diameter, wall, wall_round_up, inner_tolerance = 0) = [
  diameter + inner_tolerance, wall_round_up ? ceil(diameter + wall) : diameter +
  wall
];

/**
# Tube and Drill
Creates a tube body, or the drill for the tube body depending on the drill
argument.

This is a helper so that tubes can be overlapped with each other and still
drill clean through, see example below

# Usage
`tube_and_drill([diameter], [length], [wall], [wall_round_up], [boss_count],
                [boss_size], [drill])`

# Arguments

* `diameter` - The inner diameter of the tube. Default: `20`

* `length` - The length of the tube. Default: `20`

* `wall` - How thick the tube walls should be. Default: `3`

* `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                    the next highest integer value. For example if the
                    diameter is `3.25`, and the wall thickness is `3` then
                    the outer diameter of the tube would be `7` instead of the
                    `6.25` that it would normally be. Default: `false`

* `boss_count` - The number of screw bosses to place on either side of the
                 tube. For example, if `2` then the tube will have 4 bosses
                 total, 2 in a row, and another row of 2 180º opposite. Bosses
                 are placed evenly along the length of the tube. Default: 0

* `boss_size` - The size of the screw the screw bosses should accomodate.
                Default: 3

* `inner_tolerance` - The amount of extra slop to add to the inner diamerter.
                      Default: 0

* `drill` - If true, will generate the "drill"

# Examples
Generate a tube 25mm long with a 5mm inner diameter, and 3mm thick walls:
```scad
difference() {
  tube_and_drill(5, 25, drill=false);
  tube_and_drill(5, 25, drill=true);
}
```

Generate a tube with 3 screw bosses along its length:
```scad
difference() {
  tube_and_drill(10, 25, boss_count=3, drill=false);
  tube_and_drill(10, 25, boss_count=3, drill=true);
}

To generate two tubes intersecting each other and allowing the drills to
pass through:

```scad
difference() {
  union() {
    tube_and_drill(5, 30, 3, false, false);
    translate([-15,0,15]) {
      rotate([0,90,0]) {
        tube_and_drill(5,30, 3, false, false);
      }
    }
  }
  union() {
    tube_and_drill(5, 30, 3, false, true);
    translate([-15,0,15]) {
      rotate([0,90,0]) {
        tube_and_drill(5,30, 3, false, true);
      }
    }
  }
}
**/
module tube_and_drill(diameter = 20, length = 20, wall = 3,
                      wall_round_up = false, boss_count = 0, boss_size = 3,
                      inner_tolerance = 0, drill = false) {
  diameters = tube_diameters(diameter, wall, wall_round_up, inner_tolerance);
  if (!drill) {
    cylinder(h = length, d = diameters[1]);
  } else {
    color("red") {
      // Main tube
      translate([ 0, 0, -0.005 ]) {
        cylinder(h = length + 0.01, d = diameters[0]);
      }
    }
  }
  if (boss_count > 0 && boss_size > 0) {
    sb_height = (diameters[1] - diameters[0]) * 0.75;
    step = length / (boss_count + 1);
    pos_edge = diameters[0] / 2;
    neg_edge = -diameters[0] / 2; // - sb_height;

    for (cv = [1:boss_count]) {
      v_pos = step * cv;
      translate([ pos_edge, 0, v_pos ]) {
        rotate([ 0, 90, 0 ]) {
          screw_boss(size = boss_size, height = sb_height, drill = drill);
        }
      }
      translate([ neg_edge, 0, v_pos ]) {
        rotate([ 180, 90, 0 ]) {
          screw_boss(size = boss_size, height = sb_height, drill = drill);
        }
      }
    }
  }
}

/**
# Tube
Creates a basic hollow tube for connectors and evenly places screw bosses
along the length of the tube if requested.

If you need multiple tubes to intersect and have their drills pass through each
other properly, you should use the `tube_and_drill` method directly

# Usage
tube([diameter], [length], [wall], [wall_round_up], [boss_count], [boss_size]);

# Arguments

* `diameter` - The inner diameter of the tube. Default: `20`

* `length` - The length of the tube. Default: `20`

* `wall` - How thick the tube walls should be. Default: `3`

* `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                    the next highest integer value. For example if the
                    diameter is `3.25`, and the wall thickness is `3` then
                    the outer diameter of the tube would be `7` instead of the
                    `6.25` that it would normally be. Default: `false`

* `boss_count` - The number of screw bosses to place on either side of the
                 tube. For example, if `2` then the tube will have 4 bosses
                 total, 2 in a row, and another row of 2 180º opposite. Bosses
                 are placed evenly along the length of the tube. Default: 0

* `boss_size` - The size of the screw the screw bosses should accomodate.
                Default: 3

* `inner_tolerance` - The amount of extra slop to add to the inner diameter.
                      Default: 0
**/
module tube(diameter = 20, length = 20, wall = 3, wall_round_up = false,
            boss_count = 0, boss_size = 3, inner_tolerance = 0) {
  difference() {
    tube_and_drill(diameter, length, wall, wall_round_up, boss_count, boss_size,
                   inner_tolerance, false);
    tube_and_drill(diameter, length, wall, wall_round_up, boss_count, boss_size,
                   inner_tolerance, true);
  }
}

/**
# Screw Boss
Creates a screw boss for attaching mount screws to the inserted conduit

# Usage
screw_boss([size], [height], [drill])

# Arguments

* `size` - The size of the screw the boss is for. Default: 3

* `height` - The height of the boss. Remember that the boss will likely need to
             extend all the way through the part you're placing it on so this
             should at least be as tall as the wall thickness.
             Default: Half the outer boss diameter

* `drill` - If `true` will generate the 'drill' object for using in a difference
**/
module screw_boss(size = 3, height = 0, drill = false) {
  sb_inner_diameter = size;
  sb_outer_diameter = size + 2;
  height = height <= 0 ? sb_outer_diameter / 2 : height;

  // Calculate the "lower" outder diamer to give the boss a 45 degree
  // slope to make 3d printing easier
  diff_rad = height / tan(45);
  lower_diam = sb_outer_diameter + (2 * diff_rad);

  if (!drill) {
    cylinder(h = height, d1 = lower_diam, d2 = sb_outer_diameter);
  } else {
    color("red") {
      translate([ 0, 0, -0.1 ]) {
        cylinder(h = height + 0.2, d = sb_inner_diameter);
      }
    }
  }
}
