/**
Copyright © 2023 Tevis Money
Dual Licensed under CERN-OHL-S v2 only and AGPL v3 only

This project is free software and hardware and you should have received a
LICENSE.txt file with one or both of the above licenses when you received this
project. You may modify and distribute this project under the terms of those
licenses.

This software is available under more permissive licensing terms. Contact
licensing@ncitguys.com to for more information.
**/

use <common_components.scad>

/**
# Fabric Clamp
Creates a new clamp for attaching fabric to a conduit

# Usage
fabric_clamp([diameter], [length], [wall], [wall_round_up], [boss_size],
             [boss_count])

# Arguments

* `diameter` - The inner diameter of the clamp. This should be the exterior
               diameter of the conduit being clamped to. Default: `20`

* `length` - How long this clamp should be. Default: `20`

* `wall` - How thick the clamp walls should be. Default: `3`

* `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                    the next highest integer value. For example if the
                    diameter is `3.25`, and the wall thickness is `3` then the
                    outer diameter of the connector would be `7` instead of the
                    `6.25` that it would normally be. Default: `false`

* `inner_tolerance` - How much extra slop to add to the inner diameter.
                      Default: 0

* `clamp_edge` - If `true` then a set of tabs will be generated for clamping the
                 clip down rather than having to screw through or find another
                 way to attach to the fabric if the friction force isn't enough.
                 Default: `true`

* `boss_size` - How large of a screw the screw boss should support, set
                to　0 to remove screw bosses. Default: `3`

* `boss_count` - How many screw bosses to put on the clamp, set to 0 to remove
                 screw bosses. Default: `3`
**/

module fabric_clamp(diameter = 20, length = 20, wall = 3, wall_round_up = false,
                    inner_tolerance = 0, clamp_edge = true, boss_size = 3,
                    boss_count = 3) {
  difference() {
    main_body(diameter, length, wall, wall_round_up, inner_tolerance,
              clamp_edge, boss_size, boss_count, false);
    main_body(diameter, length, wall, wall_round_up, inner_tolerance,
              clamp_edge, boss_size, boss_count, true);
  }

  // Construct the main body, or it's drills
  module main_body(diameter, length, wall, wall_round_up, inner_tolerance,
                   clamp_edge, boss_size, boss_count, drill) {

    diameters = tube_diameters(diameter, wall, wall_round_up, inner_tolerance);
    tube_and_drill(diameter, length, wall, wall_round_up, drill = drill);
    if (clamp_edge) {
      clamp_edge(diameters, length, boss_size, boss_count, drill);
    } else {
      if (drill) {
        // Cut the clamp as if we were generating the edge
        clamp_edge(diameters, length, 0, 0, drill);
      }
      translate([ (-diameters[0] / 2) + 0.25, 0, 0 ]) {
        rotate([ 0, -90, 0 ]) {
          tube_bosses(diameters, length, boss_size, boss_count, drill);
        }
      }
    }
  }

  // Construct a clamping edge if requested
  module clamp_edge(diameters, length, boss_size, boss_count, drill) {

    abs_wall = diameters[1] - diameters[0];

    gap_shrink = diameters[0] / 2;
    long_edge = (diameters[0] + abs_wall) / 2 + (boss_size * 3);
    short_edge = diameters[0] - gap_shrink;
    short_cut = short_edge - abs_wall;
    cut_translate = abs_wall / 2;

    translate([ 0, -short_edge / 2, 0 ]) {
      if (!drill) {
        cube([ long_edge, short_edge, length ]);
      } else {
        translate([ -0.05, cut_translate, -0.05 ]) {
          cube([ long_edge + 0.1, short_cut, length + 0.1 ]);
        }
      }

      if (boss_count > 0 && boss_size > 0) {
        sb_height = (diameters[1] - diameters[0]) * 0.75;
        step = length / boss_count;
        boss_wall_pos = long_edge - (boss_size * 1.75);
        pos_edge = cut_translate;
        neg_edge = short_edge - cut_translate;

        for (cv = [1:boss_count]) {
          v_pos = step * cv;
          translate([ boss_wall_pos, pos_edge, v_pos - (step / 2) ]) {
            rotate([ 90, 0, 0 ]) {
              screw_boss(size = boss_size, height = sb_height, drill = drill);
            }
          }
          translate([ boss_wall_pos, neg_edge, v_pos - (step / 2) ]) {
            rotate([ -90, 0, 0 ]) {
              screw_boss(size = boss_size, height = sb_height, drill = drill);
            }
          }
        }

        // Cut off the extra material from the screw bosses
        if (drill) {
          translate([ 0, 0, -sb_height ]) {
            cube([ long_edge, short_edge, sb_height - 0.05 ]);
          }
          translate([ 0, 0, length ]) {
            cube([ long_edge, short_edge, sb_height + 0.05 ]);
          }
        }
      }
    }
  }

  // Generate screw bosses for the length of the tube rather than for the
  // clamping edge
  module tube_bosses(diameters, length, boss_size, boss_count, drill) {
    if (boss_count > 0 && boss_size > 0) {
      sb_height = (diameters[1] - diameters[0]) * 0.75;
      step = length / boss_count;

      for (cv = [1:boss_count]) {
        step_pos = step * cv;
        translate([ step_pos - (step / 2), 0, 0 ]) {
          screw_boss(size = boss_size, height = sb_height, drill = drill);
        }
      }

      // Cut off the extra material from the screw bosses
      cube_dim = max(diameters[1], length);
      if (drill) {
        translate([ -cube_dim, -length / 2, -0.001 ]) { cube(cube_dim); }
        translate([ length, -length / 2, -0.001 ]) { cube(cube_dim); }
      }
    }
  }
}
