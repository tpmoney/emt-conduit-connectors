/**
Copyright © 2023 Tevis Money
Dual Licensed under CERN-OHL-S v2 only and AGPL v3 only

This project is free software and hardware and you should have received a
LICENSE.txt file with one or both of the above licenses when you received this
project. You may modify and distribute this project under the terms of those
licenses.

This software is available under more permissive licensing terms. Contact
licensing@ncitguys.com to for more information.
**/

use <common_components.scad>

/**
# End mount
Creates a new end mount plate

# Usage
end_mount([diameter], [depth], [wall], [wall_round_up], [plate_diameter],
  [plate_thickness], [screw_size], [inner_tolerance])

# Arguments

* `diameter` - The inner diameter of the connector. Default: `20`

* `depth` - The "sleeve" depth for the connector, this is how much of the
            pipe being connected should fit inside the connector.
            Default: `20`

* `wall` - How thick the connector walls should be. Default: `3`

* `wall_round_up` - If `true`, then the wall thickness will be rounded up to
                    the next highest integer value. For example if the
                    diameter is `3.25`, and the wall thickness is `3` then the
                    outer diameter of the connector would be `7` instead of the
                    `6.25` that it would normally be. Default: `false`

* `plate_diamter` - The diamter of the mounting plate.
                    Default: `2.5 * ${diamter} + ${screw_size}`

* `plate_thickness` - The thickness of the mounting plate. Default: `${wall}`

* `half_open` - If true, the mount for the pipe will be cut in half to allow
                removal and placing without pulling the pipe out laterally.
                Default: `false`

* `screw_size` - How large of a screw the screw boss should support, set
                 to　0 to remove screw bosses. Default: `3`

* `inner_tolerance` - How much extra slop to add to the inner diameter.
                      Default: 0
**/
module end_mount(diameter = 20, depth = 20, wall = 3, wall_round_up = false,
                 plate_diameter = -1, plate_thickness = -1, half_open = false,
                 screw_size = 3, inner_tolerance = 0) {

  difference() {
    main_body(diameter, depth, wall, wall_round_up, plate_diameter,
              plate_thickness, half_open, screw_size, inner_tolerance, false);
    main_body(diameter, depth, wall, wall_round_up, plate_diameter,
              plate_thickness, half_open, screw_size, inner_tolerance, true);
  }

  // Construct the main body, or it's drills
  module main_body(diameter, depth, wall, wall_round_up, plate_diameter,
                   plate_thickness, half_open, screw_size, inner_tolerance,
                   drill) {

    diameters = tube_diameters(diameter, wall, wall_round_up, inner_tolerance);
    plate_diameter =
        plate_diameter < 0 ? 2.5 * diameters[0] + screw_size : plate_diameter;
    plate_thickness = plate_thickness < 0 ? wall : plate_thickness;

    // Primary tube
    screw_count = half_open ? 0 : 2;

    if (half_open) {
      tube_and_drill(diameter, depth, wall, wall_round_up, screw_count,
                     screw_size, inner_tolerance, drill);

      if (drill)
        translate([ 0, 0, 0 ]) {
          translate([ -diameters[0] / 2, 0, 0 ]) {
            cube([ diameters[0], diameters[0], depth + 0.1 ]);
          }
        }
    } else {
      tube_and_drill(diameter, depth, wall, wall_round_up, screw_count,
                     screw_size, inner_tolerance, drill);
    }
    translate([ 0, 0, -plate_thickness - 0.01 ]) {
      end_plate(plate_diameter, plate_thickness, screw_size, drill);
    }
  }

  // Construct the end plate or its drills
  module end_plate(diameter, thickness, screw_size, drill) {
    boss_shift = diameter / 3;
    boss_pos = [
      [ -boss_shift, 0, 0 ], [ boss_shift, 0, 0 ], [ 0, -boss_shift, 0 ],
      [ 0, boss_shift, 0 ]
    ];
    if (!drill) {
      cylinder(h = thickness, d = diameter);
    }
    for (cv = [0:3])
      translate(boss_pos[cv]) { screw_boss(screw_size, thickness, drill); }
  }
}
